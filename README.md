# Automating Jenking With Groovy

.footer: Created By Alex M. Schapelle, VAIOLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Groovy ?
- Who needs Groovy ?
- How Groovy works with Jenkins?
- How to share code in various scenarios between scripts and pipelines ?


### Who Is This course for ?

- Junior/senior ops/admins who have __solid knowledge__ of Jenkins and wish to extend it.
- For junior/senior developers who are developing complex stacks and wish to customize their pipelines.
- Experienced dev/ops who need refresher on shared libs and custom pipelines

---

# About The Course Itself ?

### Several Assumptions:

- You know how to install and configure Jenkins on Server or Container/Compose.
- You grasp the knowledge of accessing and configuring Jenkins.
- You posses a understading of Jenkins plugins.
- You know how to create and use declarative pipelines.
- _You should have some knowledge of any scripting language: Python, Ruby, JavaScript..._
  - __Or you should have deep understanding of UNIX/Linux Shell scripting.__

---

# Course Topics

- Intro
- Groovy Fundamentals
- Working With Jenkins
- Creating Builds
- Shared Libraries
- Managing Users and Credentials