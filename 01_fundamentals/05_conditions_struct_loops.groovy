#!/usr/bin/env groovy

int numCount = 10
def hero_group = "Justice League"
String[] heros = ["batman", "bluebeetle", "flash", "elasticman", "greemlantern"]

for (int i = 0 ; i < numCount; i++){
    println i 
}

int i = 0
while (i < numCount){
    println i
    i++
}

for (hero in heros){
    println hero
}

for (letter in hero_group){
    println letter
}