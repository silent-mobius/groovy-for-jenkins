
---

# Introduction to Groovy

---

# Introduction to Groovy
Groovy is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax.

Although we will learn Groovy, we'll mostly focus on most meaningful and useful parts of the language, mostly because the course is dedicated to Groovy in conjunction with Jenkins and their usability together, this all parts of Groovy are not required.
---

# Groovy Console

To use groovy we first must learn its basic, and although our target working environment is Jenkins, we still need to go over the basics of the language, and later on use it in Jenkins pipelines.

To get groovy on your system there are several ways to do so:

- [sdkman.io](https//sdkman.io) install utility that enables to install and configure software development tools, on your system
- [apache-groovy](https://groovy.apache.org/download.html) download page where we can install binaries directly

```sh
curl -sL "https://get.sdkman.io" | bash 
apt-get install default-sdk -y
sdk install groovy
groovysh # this should open groovy shell -> if not: ask for help
```
Once all is installed, we can use Groovy dedicated development environment called GroovyConsole to learn Groovy essentials to later incorporate them in side our jenkins pipeline code.

For now let us use `groovyConsole` to develop our initial Groovy capabilities

> `[!]` Note: if you are using VsCode/Geany/Any type of IDE or Extended Text editor, there is NO NEED to use `GroovyConsole`.
> Just verify that your IDE/Text editor recognizes the groovy shell and groovy binaries

Lets expand our example with additional code:

```groovy 

def x = 5

x += 5

println x

```

Let's go over the code and explain what is going on there.

---

# Practice

### Groovy Console

- Install sdkman tool on your system
- install Java sdk on your system
- Using sdkman utility called `sdk` install groovy on your system
- Enter groovy shell with `groovysh`
- Run in groovy shell next command:
```groovy
println("Hello World!")
```
- Open GroovyConsole or your Text


---

# Data Types

In every programming language there are initial data type as well as programming language definition as well, thus we should dwell on the definition and those initial data types:
- Groovy is described as __Optionally Typed Language__, meaning that Groovy run time environment, AKA Groovy shell can guess the type of the data that we are providing to it.
- Groovy support 4 main data type across most of programming languages:
    - Strings: collection of characters
    - Integers: decimal numbers
    - Floats: 
    - Booleans; True/False
- Each of the data types can be declared with `def` or `var` placeholder keywords, making Groovy to detect data types automatically, when you do not want to give an explicit type. 

### Variable Assignment

You can assign values to variables for later use, placing variable name on the left and assigning the value on the right. For assignment to work you need to use assignment operator `=`, for example:

```groovy
var name = "Alex M. Schapelle"
def job = "DevOps Engineer"  // Whether you use var or def does not matter, but you need to be consistent for the code to be readable
```
Groovy support multiple variable assignment, just line Python does. e.g

```groovy 
def (a,b,c) = [10,20.2,'Alex']
// We can test the values with assert command
assert a == 10 && b == 20.2 && c == 'Alex'
// == is comparison operator
```

> `[!]` Note: if not enough values are provided during multiple variable assignment, excess ones are filled with `null`
> and in case there are too many variables but not enough values, the extra variables are __ignored__

While many variables can keep values inside of them, they can be presented with `println` command, which prints the string and new line including the values that variables are storing. Let's take a look:

```groovy
var name =" Alex M. Schaplle"
var answerToUniverse = 42
def salary = 99999.98
def isDevOps = True

println "My name is :" + name + "I believe that answer to Universe riddle is: " + answerToUniverse
println "Answer to you question whether I am  DevOps is: "+ isDevOps + "and the Salary of "+salary+ "is mostly taken away from us in TAXES"

```

> `[!]` Note: `+` is considered as concatenation operator
> We'll go over operators in short later on.

While working with strings, let us remember that strings are considered to be objects in groovy, thus we can implement any number of [string methods](https://docs.groovy-lang.org/latest/html/api/org/codehaus/groovy/runtime/StringGroovyMethods.html) that we can find in documentation.

One thing that can expended with __Groovy__ is that also can be written as a static language which can make it easier to read for developers

```groovy 

String name =" alex M. Schaplle"
Int answerToUniverse = 42
float salary = 99999.98
Boolean isDevOps = True

println "My name is : " + name,capitalize() + " I believe that answer to Universe riddle is: " + answerToUniverse.to_floa
println "Answer to you question whether I am  DevOps is: "+ isDevOps + "and the Salary of "+salary+ " is mostly taken away from us in TAXES"

```

Other data types are also considered as object, each and every one can have many methods to its own object, here are examples as follows:
- `toString()`: convert any data type to become string
  - `trim()`
  - `capitalize()`
- `toInteger()`: convert numeric string or float to become integer
- `toBigDecimal()` : convert numeric string or int to become float --> somewhat tricky
- `toBoolean()`: convert integer or string to become boolean value of `true` or `false`

---

# Practice

### Data Types

- Create  `data_types_practice.groovy` file
- By use of `def` keyword to define next list of variables with values"
    - startup_name = "VaioLabs"
    - product = "ogun"
    - version = "1.3.86"
    - url = "https://gitlab.com/silent-mobius/ogun"
- Print all variables

---

# Control Structure

To automate decision making and  iteration in any programming language, control structures are implemented. Groovy is not stranger to this and it provides us with two main  ways to implement control structures:

- Conditions
- Loops

## Conditions

In Groovy, just line in Java, conditions are managed byt `if - else` keywords.
#### if else

```groovy 
if (isDevOps){
  println "He is DevOps Engineer"
}
else {
  println "Not everyone are perfect"
}
```

#### Nested Conditions `if else if`
We can add list of conditional checks with `else if` after first condition check and continue the condition evaluation till the required part is reached.

```groovy
if (!isDevOps){
  println "He is DevOps Engineer"
} else if ((knowsProgramming) && (knowsSystem)){
  println "Sounds like DevOps to me"
}
else {
  println "Not everyone can be perfect"
}
```

> `[!]` Note" && is a logical operator of `and` where 2 variables of `true` will return you `true`
>  the `!` is an negation operator, also is called `not` operator -> converts `true` to false


## Loops

So far, we have seen statements which have been executed one after the other in a sequential manner. Additionally, statements are provided in Groovy to alter the flow of control in a program's logic. They are then classified into flow of control statements which we will see in detail.

#### for  

Groovy supports C/Java style for loop

```groovy
var numCount = 10

for (int i = 0 ; i< numCount; i++){
  println i, (i+1)
}
```

Groovy can also use `in` keyword to iterate over the collection of data, whether it is _string_ or _list/hashmap/dictionary_

```groovy
String[] heros = ["batman", "bluebeetle", "flash", "elasticman", "greenlantern"]

for (hero in heros){
    println hero
}
```

#### while

The while statement is executed by first evaluating the condition expression (a Boolean value), and if the result is true, then the statements in the while loop are executed

```groovy
def x = 0
def y = 5

while ( y-- > 0 ) {
    x++
}
```

## Operators

Groovy supports the usual familiar arithmetic operators you find in mathematics and in other programming languages like Java/Python. Out of All the operators that are supported, listed below are most common and required to use:
- Arithmetic: `+` `-` `*` `/` `%` `**` `++` `--`
    - `%` reminder
    - `**` power
- Assignment: `+=` `-=` `*=` `/=` `%=` `**=`
- Relational: `==` `!=` `<` `>` `<=` `>=` `===` `!==`
    - `===` identical, meaning same value and same data type __from the same object or instance__
    - `!==` not identical, meaning that might be same value and type __but NOT from same object or instance__
- Logical: `&&` `||` `!`
    - `!` : The logical "not" has a higher priority than the logical "and".

---

# Practice

### Control Structures
---
# Data Structures

## Collections and Maps

Let’s take a look at how some basic data structures are handled


---
# Subroutines

Subroutines also known as functions, are the most basic tool for code reuse in Groovy.

funny enough, `def` keyword is also used to define functions in Groovy, after which provide the name of the function and then in parentheses the list of expected parameters. In our first example there are no parameters.

Then within curly braces we put the body of the function.

We can put the definition of the function anywhere in the file. A good practice would be to put all the functions in the end of the file and put the main body of the code at the top. That will make it easier to read.

Better yet, you might want to put all your code in functions and leave only a single call to a single function in the main body of your code. That will probably help keeping the code clean. 

```groovy
def add(x, y) {
    return x+y
}

z = add(2, 3)
println(z)   // 5
```
---

# Practice

---

# Classes and Objects

- Base level of encapsulation
- Wrap your code in classes
- Parallel  lists => object with two properties
- Very similar to Java or C#

```groovy
class User {}{
    String lastName;
    String firstName;
    public String  UserName(){
      return getUserName(this.firstName, this.lastName);
    }
    private String getUserName(String  firstName, String lastName){
        return firstName.toLowerCase().capitalize() lastName.toLowerCase().capilalize()
    }
}

User ninja = new User()
```
---

# Practice

---

# Inheritance 

---

# Practice


----

# External  Packages

---

# Practice 

---

# Summary

---
# Summary Practice