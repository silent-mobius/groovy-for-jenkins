#!/usr/bin/env groovy


var isDevOps = true
var knowsProgramming = true
var knowsSystem = true

if (isDevOps){
  println "He is DevOps Engineer"
}
else {
  println "Not everyone are perfect"
}

if (!isDevOps){
  println "He is DevOps Engineer"
}
else {
  println "Not everyone can be perfect"
}

if (!isDevOps){
  println "He is DevOps Engineer"
} else if ((knowsProgramming) && (knowsSystem)){
  println "Sounds like DevOps to me"
}
else {
  println "Not everyone can be perfect"
}
