#!/usr/bin/env groovy

class User{
    String lastName;
    String firstName;
    public String  UserName(){
      return getUserName(this.firstName, this.lastName);
    }
    private String getUserName(String  firstName, String lastName){
        return firstName.toLowerCase().capitalize() + " "+lastName.toLowerCase().capitalize();
    }
}



String[] firstNames = ["John", "Bruce", "Wolly"]
String[] lastNames = ["Ja'haans", "Wayne", "West"]

for (int i = 0; i < firstNames.size(); i++){
    User u = new User(firstName: firstNames[i], lastName: lastNames[i]);
    println("Username  is ${u.UserName()}");
}