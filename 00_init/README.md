# Automating Jenking With Groovy

.footer: Created By Alex M. Schapelle, VAIOLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Groovy ?
- Who needs Groovy ?
- How Groovy works with Jenkins?
- How to share code in various scenarios between scripts and pipelines ?


### Who Is This course for ?

- Junior/senior ops/admins who have __solid knowledge__ of Jenkins and wish to extend it.
- For junior/senior developers who are developing complex stacks and wish to customize their pipelines.
- Experienced dev/ops who need refresher on shared libs and custom pipelines

---

# About The Course Itself ?

### Several Assumptions:

- You know how to install and configure Jenkins on Server or Container/Compose.
- You grasp the knowledge of accessing and configuring Jenkins.
- You posses a understading of Jenkins plugins.
- You know how to create and use declarative pipelines.
- _You should have some knowledge of any scripting language: Python, Ruby, JavaScript..._
  - __Or you should have deep understanding of UNIX/Linux Shell scripting.__

---

# Course Topics

- Intro
- Groovy Fundamentals
- Working With Jenkins
- Creating Builds
- Shared Libraries
- Managing Users and Credentials

---

# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
  - Between each semester, I tried to take IT course at various places.
    - A+.
    - Cisco CCNA.
    - RedHat RHCSA.
    - LPIC1 and Shell scripting.
    - Other stuff I've learned alone.

---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---
# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourself:

- Name and Surname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins / Groovy / Gradle
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash/PowerShell Script
    - Python3 / Pytest / Pylint / Flask
    - Go / Gin / Echo
    - C / Make
    - Java / Maven / POM.XML 
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---

# 00 History

__Groovy__ is a __Java-syntax-compatible object-oriented__ programming language for the __Java platform__. It is both a static and dynamic language with features similar to those of Python, Ruby, and Smalltalk. It can be used as both a programming language and a scripting language for the __Java__ Platform, is compiled to Java virtual machine (JVM) bytecode, and interoperates seamlessly with other Java code and libraries. Groovy uses a curly-bracket syntax similar to __Java's__. __Groovy__ supports closures, multiline strings, and expressions embedded in strings. Much of Groovy's power lies in its AST transformations, triggered through annotations.

__Groovy__ 1.0 was released on January 2, 2007, and __Groovy__ 2.0 in July, 2012. Since version 2, __Groovy__ can be compiled statically, offering type inference and performance near that of Java.Groovy 2.4 was the last major release under Pivotal Software's sponsorship which ended in March 2015. __Groovy__ has since changed its governance structure to a Project Management Committee in the Apache Software Foundation.

